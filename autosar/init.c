/*
 * 础光实时操作系统PhotonRTOS -- AUTOSAR初始化
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Zicheng Hu <huzicheng@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#include <autosar/internal.h>
#include <photon/init_integrity.h>

/**
 * AUTOSAR 预先初始化，在调用StartOS之前需要初始化一些基础变量
 */
FUNC(void, OS_CODE) autosar_preinit(void)
{
	/**
	 * 初始化核相关代码
	 */
	MODULE_INIT(autosar_preinit);
	autosar_multicore_preinit();
}
