/*
 * PhotonRTOS础光实时操作系统 -- AUTOSAR对象类型
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Jiayuan Liang <liangjiayuan@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef OS_OBJECT_TYPES_H
#define OS_OBJECT_TYPES_H

#include <autosar/common_types.h>
#include <autosar/os_application_types.h>

typedef VAR(uint8, TYPEDEF) ObjectAccessType;
typedef VAR(uint8, TYPEDEF) ObjectTypeType;

struct autosar_object {
	VAR(ApplicationType, TYPEDEF) app_id;
	/**
	 * 在CallTrustedFunction()的上下文中，app_id应该为trusted_func规定的trusted_func_app_id。
	 */
	VAR(ApplicationType, TYPEDEF) trusted_func_app_id;
	VAR(ApplicationType, TYPEDEF) accessable_application_id[AUTOSAR_NR_APPLICATION];
	VAR(ObjectTypeType, TYPEDEF) object_type;
};

typedef VAR(struct autosar_object, TYPEDEF) OsObjectType;
typedef P2VAR(OsObjectType, TYPEDEF, OS_APPL_DATA) OsObjectRefType;

#endif /* OS_OBJECT_TYPES_H */
