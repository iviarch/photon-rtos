/*
 * PhotonRTOS础光实时操作系统 -- AUTOSAR闹钟接口
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Baoyou Xie <xiebaoyou@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef OS_ALARM_H
#define OS_ALARM_H

#include <autosar/alarm_types.h>
#include <autosar/alarm_config.h>
#include <autosar/system_types.h>


/**
 * 该系统服务终止警报<AlarmID>。
 * 语法：
 *     StatusType CancelAlarm ( AlarmType <AlarmID> )
 * 参数（输入）：
 *     AlarmID		 对警报的引用
 * 参数（输出）：
 *     无
 * 特殊说明：
 *     允许在任务级、ISR中使用，
 *     但是不允许在钩子例程中使用。
 * 状态：
 *     标准：
 *         无错误，E_OK
 *         警报<AlarmID>不在使用中，E_OS_NOFUNC
 *     扩展：
 *         警报<AlarmID>无效，E_OS_ID
 * 一致性：
 *     BCC1, BCC2, ECC1, ECC2
 */
extern FUNC(StatusType, OS_CODE) CancelAlarm(
	VAR(AlarmType, AUTOMATIC) AlarmID);

/**
 * 返回以滴答表示的、警报<AlarmID>超时值。
 * 语法：
 *     StatusType GetAlarm ( AlarmType <AlarmID>, TickRefType <Tick>)
 * 参数（输入）：
 *     AlarmID	对警报的引用
 *  参数（输出）：
 *     Tick		以滴答表示的，在警报<AlarmID>
 *              超时相对值(相对于当前时间)
 * 特殊说明：
 *     由应用程序（例如 CancelAlarm）决定是否仍然有用。
 *     如果<AlarmID>不在使用中，<Tick>未定义。
 *     允许在任务级、
 *     ISR以及几个钩子例程中调用（参见图12.1）
 * 状态：
 *     标准：
 *        无错误，E_OK
 *        警报<AlarmID>未在使用中，E_OS_NOFUNC
 *    扩展：
 *        警报<AlarmID>无效，E_OS_ID
 * 一致性：
 *     BCC1, BCC2, ECC1, ECC2
 */
extern FUNC(StatusType, OS_CODE) GetAlarm(
	VAR(AlarmType, AUTOMATIC) AlarmID,
	VAR(TickRefType, AUTOMATIC) Tick);

/**
 * 读取警报的base特征。
 * 返回值<Info>是一个数据结构，
 * 在该结构中存储AlarmBaseType类型的信息。
 * 语法：
 *     StatusType GetAlarmBase ( AlarmType <AlarmID>, AlarmBaseRefType <Info> )
 * 参数（输入）：
 *     AlarmID			对警报的引用
 * 参数（输出）：
 *     info				对警报base常量值的引用
 * 特殊说明：
 *     允许在任务级、ISR以及几个钩子例程中（参见图12.1）。
 * 状态：
 *     标准：
 *        无错误，E_OK
 *     扩展：
 *        警报< AlarmID>无效，E_OS_ID
 *  一致性：
 *     BCC1, BCC2, ECC1, ECC2
 */
extern FUNC(StatusType, OS_CODE) GetAlarmBase(
	VAR(AlarmType, AUTOMATIC) AlarmID,
	VAR(AlarmBaseRefType, AUTOMATIC) Info);

/**
 * 该系统服务占据警报<AlarmID>
 * 在<start>滴答到达后，与警报<AlarmID>相关的任务被激活
 * 或者相关事件（仅扩展任务）被设置
 * 或者警报回调函数被调用。
 * 语法：
 *     StatusType SetAbsAlarm ( AlarmType <AlarmID>,
 *             TickType <start>,
 *             TickType <cycle> )
 * 参数（输入）：
 *     AlarmID	对警报的引用
 *     start	以滴答表示的绝对值
 *     cycle	在循环警报的情况下，表示循环次数。
 *				在单次警报情况下，其值应当为0
 * 参数（输出）：
 *     无
 * 特殊说明：
 *     如果绝对值<start>非常接近于当前计数值，
 *		警报可能会在系统服务返回用户时超时
 *     并且任务可能变为就绪状态，
 *     或者警报回调可能被调用。
 *     如果在调用系统调用前，绝对值<start>已经到达，
 *	   那么警报仅仅应当在绝对值再次到达的时候到期。
 *     也就是说，在下一次溢出计数器的之后。
 *     如果 <cycle> 不等于零，
 *		则到期后立即使用相对值 <cycle>再次记录警报。
 *     警报<AlarmID>不应当在使用中。
 *     要修改已经在使用中的警报值，应当首先中止它。
 *     如果警报已经在用，该调用将被忽略，
 *		并且返回错误E_OS_STSTE。
 *     允许在任务级、ISR中使用，
 *     但是不允许在钩子例程中使用。
 * 状态：
 *     标准：
 *        无错误，E_OK
 *        警报<AlarmID>已经在使用中，E_OS_STATE
 *     扩展：
 *        警报<AlarmID>无效，E_OS_ID
 *        <start> 的值超出允许限制
 *        （小于零或大于 maxallowedvalue），E_OS_VALUE
 *        <cycle> 的值不等于 0 且超出允许的计数器限制
 *        （小于 mincycle 或大于 maxallowedvalue），E_OS_VALUE
 * 一致性：
 *     BCC1, BCC2, ECC1, ECC2
 */
extern FUNC(StatusType, OS_CODE) SetAbsAlarm(
	VAR(AlarmType, AUTOMATIC) AlarmID,
	VAR(TickType, AUTOMATIC) Start,
	VAR(TickType, AUTOMATIC) Cycle);

/**
 * 占用警报<AlarmID>
 * 在<increment>次滴答过去后，
 * 与警报<AlarmID>相关的任务被激活
 * 或者相关事件（仅扩展任务）被设置，
 * 或者警报回调函数被调用。
 * 语法：
 *     StatusType SetRelAlarm ( AlarmType <AlarmID>,
 *          TickType <increment>,
 *          TickType <cycle> )
 * 参数（输入）：
 *     AlarmID		对警报的引用
 *     increment	以滴答表示的相对值
 *     cycle		在循环警报的情况下，表示循环次数。
 *					在单次警报情况下，
 *					其值应当为0
 * 参数（输出）：
 *     无
 * 特殊说明：
 *     <increment> 等于 0 的行为取决于实现。
 *     如果相对值 <increment> 非常小
 *     那么在系统服务返回之前，警报可能会过期，
 *     并且任务可能变为就绪状态，
 *     或者警报回调函数可能被调用。
 *     该系统服务占用警报<AlarmID>，
 *     在<increment>次滴答过去后，
 *     与警报<AlarmID>相关的任务被激活或者
 *        相关事件（仅扩展任务）被设置，
 *        或者警报回调函数被调用。
 *     如果 <cycle> 不等于零，
 *     则警报在到期后立即以相对值 <cycle> 再次运行。
 *     警报<AlarmID>必须不在使用中。
 *     要改变已经在用的警报值，应当首先中止它。
 *     如果警报已经在使用中，该调用将被忽略，
 *     并且返回E_OS_STATE。
 *     允许在任务级、ISR中调用，但是不允许在钩子例程中。
 * 状态：
 *     标准：
 *        无错误，E_OK
 *        警报<AlarmID>已经在使用中，E_OS_STATE
 *     扩展：
 *        警报<AlarmID>无效，E_OS_ID
 *        <increment> 的值超出允许限制
 *        （小于零或大于 maxallowedvalue），E_OS_VALUE
 *        <cycle> 的值不等于 0 且超出允许的计数器限制
 *        （小于 mincycle 或大于 maxallowedvalue），E_OS_VALUE
 * 一致性：
 *     BCC1, BCC2, ECC1, ECC2
 */
extern FUNC(StatusType, OS_CODE) SetRelAlarm(
	VAR(AlarmType, AUTOMATIC) AlarmID,
	VAR(TickType, AUTOMATIC) Increment,
	VAR(TickType, AUTOMATIC) Cycle);

#endif /* OS_ALARM_H */
