/*
 * PhotonRTOS础光实时操作系统 -- AUTOSAR闹钟类型
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Baoyou Xie <xiebaoyou@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef OS_ALARM_TYPES_H
#define OS_ALARM_TYPES_H

#include <autosar/common_types.h>
#include <autosar/counter_types.h>

typedef VAR(uint8, AUTOMATIC) AlarmType;
/**
 * 警报周期：
 *     maxallowedvalue：
 *        警报周期的最大滴答值，
 *        超过此值可能导致溢出
 *     ticksperbase：
 *        达到特定计数器所需的滴答数
 *     mincycle:
 *        cycle参数的最小允许值
 *        （仅适用于具有扩展状态的系统）
 */
typedef struct {
	VAR(TickType, TYPEDEF) maxallowedvalue;
	VAR(TickType, TYPEDEF) ticksperbase;
	VAR(TickType, TYPEDEF) mincycle;
} AlarmBaseType;

struct osek_alarm_attr {
	/**
	 * 回调函数
	 */
	P2FUNC(void, TYPEDEF, callback) (void);
	/**
	 * 应用模式
	 */
	VAR(AppModeType, TYPEDEF) app_mode;
	VAR(uint8, AUTOMATIC) counterID;
};

typedef P2VAR(AlarmBaseType,TYPEDEF,OS_APPL_DATA) AlarmBaseRefType;

#endif /* OS_ALARM_TYPES_H */
