/*
 * PhotonRTOS础光实时操作系统 -- AUTOSAR计数器类型
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Lin Yang <s-yanglin@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef OS_COUNTER_TYPES_H
#define OS_COUNTER_TYPES_H

#include <autosar/common_types.h>

typedef VAR(uint32, TYPEDEF) TickType;
typedef P2VAR(TickType, TYPEDEF,OS_APPL_DATA) TickRefType;

typedef VAR(uint8, TYPEDEF) CounterType;

#endif /* OS_COUNTER_TYPES_H */
