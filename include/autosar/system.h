/*
 * PhotonRTOS础光实时操作系统 -- AUTOSAR系统接口
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Zicheng Hu <huzicheng@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef OS_SYSTEM_H
#define OS_SYSTEM_H

#include <autosar/system_types.h>

/**
 * 该服务返回当前应用模式。它可用于编写与模式相关的代码。
 * 语法：
 *     AppModeType GetActiveApplicationMode ( void )
 * 参数（输入）：
 *     无
 * 参数（输出）：
 *     无
 * 特殊说明：
 *     应用模式主要用于测试、生产等不同场景。
 *     允许在任务、ISR以及所有钩子例程中调用。
 * 状态：
 *     标准：
 *        无
 *     扩展：
 *        无
 * 一致性：
 *        BCC1, BCC2, ECC1, ECC2
 */
extern FUNC(AppModeType, OS_CODE) GetActiveApplicationMode(void);

/**
 *  用户可以调用该系统服务以终止整个系统（例如紧急关机）。
 * 操作系统也在内部调用该函数，
 * 如果它到达未定义内部状态并且不再准备运行。
 *  如果ShutdownHook被配置，
 * ShutdownHook例程在关闭操作系统前
 *   总是被调用（以<Error>作为参数）。
 *  如果ShutdownHook返回，更多ShutdownOS的行为是实现特定的。
 * 在OSEK OS和OSEKtime OS共存时，ShutdownHook必须返回。
 *  <Error>必须是一个OSEK OS支持的有效错误代码，
 * 在OSEK OS和OSEKtime OS共存时，
 * <Error>也可以是OSEKtime OS所接受的值。
 * 在这种情况下，如果被OSEKtime配置参数打开，
 * OSEKtime OS将在OSEK OS关闭后关闭。
 * 语法：
 *     void ShutdownOS ( StatusType <Error> )
 * 参数（输入）：
 *     Error				错误发生
 * 参数（输出）：
 *     无
 * 特殊说明：
 *     在该服务调用后，操作系统被关闭。
 *     允许任务、ISR、ErrorHook、StartupHook以及
 *     在操作系统内部调用。
 * 状态：
 *     标准：
 *        无
 *     扩展：
 *        无
 * 一致性：
 *     BCC1, BCC2, ECC1, ECC2
 */
extern FUNC(void, OS_CODE) ShutdownOS(
	VAR(StatusType, AUTOMATIC) Error);

/**
 * 用户可以调用该系统服务以特定模式启动操作系统。
 * 语法：
 *     void StartOS ( AppModeType <Mode> )
 * 参数（输入）：
 *     mode				应用模式
 * 参数（输出）：
 *     无
 * 特殊说明：
 *     仅允许在操作系统之外使用，
 *     因此可能会实施特定的实现限制。
 * 状态：
 *     标准：
 *        无
 *     扩展：
 *        无
 * 一致性：
 *     BCC1, BCC2, ECC1, ECC2
 */
extern FUNC(void, OS_CODE) StartOS(
    VAR(AppModeType, AUTOMATIC) Mode);

/**
 * 返回当前系统日志缓冲区信息。
 * 语法：
 *     StatusType GetSysLog ( void )
 * 参数（输入）：
 *     缓冲区，缓冲区大小，清除系统缓冲区标志
 * 参数（输出）：
 *     系统日志
 * 特殊说明：
 *     允许在任务级、ISR级以及内部钩子例程调用。
 *     该服务目的是用在库函数和钩子例程中。
 * 状态：
 *     标准：
 *        没有错误，E_OK
 *     扩展：
 *        没有错误，E_OK
 * 一致性：
 *     BCC1, BCC2, ECC1, ECC2
 */

extern FUNC(StatusType, OS_CODE) GetSysLog(
	VAR(int8, AUTOMATIC) buf[],
	VAR(uint32, AUTOMATIC) size,
	VAR(boolean, AUTOMATIC) buf_clear);

#endif /* OS_SYSTEM_H */
