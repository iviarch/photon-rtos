/*
 * PhotonRTOS础光实时操作系统 -- AUTOSAR计数器接口
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Lin Yang <s-yanglin@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef OS_COUNTER_H
#define OS_COUNTER_H

#include <autosar/counter_types.h>

/*
 * 语法
 * StatusType IncrementCounter (
 *      CounterType CounterID
 * )
 *
 * 服务 ID [hex]
 *      0x0f
 *
 * 同步/异步
 *      同步, 可能会导致重新调度
 *
 * 可重入性
 *      可重入
 *
 * 参数 (in)
 *      CounterID
 *      要递增的计数器
 *
 * 参数 (inout)
 *      None
 *
 * 参数 (out)
 *      None
 *
 * 返回值
 *      StatusType
 *      E_OK：没有错误
 *      E_OS_ID（仅在 EXTENDED 状态下）：CounterID 无效或计数器在硬件中实现且
 * 不能由软件递增
 *
 * 描述
 *      该服务递增一个软件计数器的值。
 *
 * 引用
 *      AUTOSAR OS 8.4.17 IncrementCounter [SWS_Os_00399]
 */
FUNC(StatusType, OS_CODE) IncrementCounter(
	VAR(CounterType, AUTOMATIC) CounterID);

/*
 * 语法
 * StatusType GetCounterValue (
 *      CounterType CounterID,
 *      TickRefType Value
 * )
 *
 * 服务 ID [hex]
 *      0x10
 *
 * 同步/异步
 *      同步（Synchronous）
 *
 * 可重入性
 *      可重入
 *
 * 参数 (in)
 *      CounterID
 *      应读取计数值的计数器
 *
 * 参数 (inout)
 *      None
 *
 * 参数 (out)
 *      Value
 *      包含计数器的当前计数值
 *
 * 返回值
 *      StatusType
 *      E_OK：没有错误
 *      E_OS_ID（仅处于 EXTENDED 状态）：<CounterID> 不是有效的
 *
 * 描述
 *      此服务读取计数器的当前计数值（如果计数器由硬件驱动，则返回硬
 * 件计时器tick，或者当用户驱动计数器时返回软件tick）。
 *
 * 引用
 *      AUTOSAR OS 8.4.18 GetCounterValue [SWS_Os_00383]
 */
FUNC(StatusType, OS_CODE) GetCounterValue(
	VAR(CounterType, AUTOMATIC) CounterID,
	VAR(TickRefType, AUTOMATIC) Value);

/*
 * 语法
 * StatusType GetElapsedValue (
 *      CounterType CounterID,
 *      TickRefType Value,
 *      TickRefType ElapsedValue
 * )
 *
 * 服务 ID [hex]
 *      0x11
 *
 * 同步/异步
 *      同步（Synchronous）
 *
 * 可重入性
 *      可重入
 *
 * 参数 (in)
 *      CounterID
 *      要读取的计数器
 *
 * 参数 (inout)
 *      Value
 *      in：之前读取的计数器的刻度值
 *      out：计数器的当前刻度值
 *
 * 参数 (out)
 *      ElapsedValue
 *      与上一次读取值的差异
 *
 * 返回值
 *      StatusType
 *      E_OK：没有错误
 *      E_OS_ID :（仅处于 EXTENDED 状态）：CounterID 无效
 *      E_OS_VALUE :（仅处于 EXTENDED 状态）：给定的值无效
 *
 * 描述
 *      此服务获取当前刻度值与先前读取的刻度值之间的刻度数。
 *
 * 引用
 *      AUTOSAR OS 8.4.19 GetElapsedValue [SWS_Os_00392]
 */
FUNC(StatusType, OS_CODE) GetElapsedValue(
	VAR(CounterType, AUTOMATIC) CounterID,
	VAR(TickRefType, AUTOMATIC) Value,
	VAR(TickRefType, AUTOMATIC) ElapsedValue);

#endif /* OS_COUNTER_H */
