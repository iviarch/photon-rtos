/*
 * PhotonRTOS础光实时操作系统 -- AUTOSAR钩子类型
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Baoyou Xie <xiebaoyou@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef OS_HOOKS_TYPES_H
#define OS_HOOKS_TYPES_H

#include <autosar/common_types.h>

#endif /* OS_HOOKS_TYPES_H */
