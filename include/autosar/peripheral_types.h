/*
 * PhotonRTOS础光实时操作系统 -- AUTOSAR外设类型
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Yili Zhang <s-zhangyili@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef OS_PERIPHERAL_TYPES_H
#define OS_PERIPHERAL_TYPES_H

#include <autosar/common_types.h>

typedef VAR(uint16, TYPEDEF) AreaIdType;

#endif /* OS_PERIPHERAL_TYPES_H */
