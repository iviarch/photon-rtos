/*
 * PhotonRTOS础光实时操作系统 -- AUTOSAR应用类型
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Jiayuan Liang <liangjiayuan@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef OS_APPLICATION_TYPES_H
#define OS_APPLICATION_TYPES_H

#include <autosar/common_types.h>
#include <autosar/const.h>
#include <autosar/config.h>

typedef VAR(uint32, TYPEDEF) ApplicationType;
typedef VAR(uint8, TYPEDEF) ApplicationStateType;
typedef P2VAR(ApplicationStateType, TYPEDEF, OS_APPL_DATA) ApplicationStateRefType;

typedef VAR(uint8, TYPEDEF) TrustedFunctionIndexType;
typedef P2VAR(void, TYPEDEF, OS_APPL_DATA) TrustedFunctionParameterRefType;
typedef VAR(int32, TYPEDEF) AccessType;

typedef VAR(uint8, TYPEDEF) RestartType;

#ifdef AUTOSAR_OS_APPLICATION

#define AUTOSAR_NR_APPLICATION CONFIG_AUTOSAR_APPLICATION_COUNT

#else

#define AUTOSAR_NR_APPLICATION 0

#endif

#define INVALID_OSAPPLICATION AUTOSAR_NR_APPLICATION

#endif /* OS_APPLICATION_TYPES_H */
