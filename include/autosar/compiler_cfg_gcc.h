/*
 * PhotonRTOS础光实时操作系统 -- AUTOSAR编译器抽象gcc
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Zicheng Hu <huzicheng@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef OS_AUTOSAR_COMPILER_CFG_GCC_H
#define OS_AUTOSAR_COMPILER_CFG_GCC_H

/* Inline functions */
#define OS_INLINE inline
/* Static Inline functions */
#define OS_STATIC_INLINE static inline

#endif /* OS_AUTOSAR_COMPILER_CFG_GCC_H */
