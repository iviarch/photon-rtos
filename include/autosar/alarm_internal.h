/*
 * PhotonRTOS础光实时操作系统 -- AUTOSAR闹钟内部实现
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Baoyou Xie <xiebaoyou@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef OS_ALARM_INTERNAL_H
#define OS_ALARM_INTERNAL_H

#include <autosar/alarm.h>

#include <autosar/calllevel_internal.h>
#include <autosar/object_internal.h>
#include <autosar/counter_internal.h>

struct osek_alarm {
	/**
	 * 必须放在结构体的首部，代表alarm的父类
	*/
	VAR(struct autosar_object,TYPEDEF) objecct;
	/**
	 * 警报是否处于激活状态
	 */
	VAR(int32_t, TYPEDEF) activated;
	/**
	 * 警报到期回调函数
	 */
	P2FUNC(void, TYPEDEF, callback)(void);

	/**
	 * 定时器绑定的计数器
	 */
	VAR(CounterType, TYPEDEF) counterID;
	/**
	 * 绑定到计数器驱动alarm的waiter
	 */
	VAR(struct autosar_counter_waiter, TYPEDEF) waiter;

};
extern VAR(struct osek_alarm, AUTOMATIC) osek_alarms[];

FUNC(void, OS_CODE) __osek_alarm_callback(
	P2VAR(struct autosar_counter_waiter,AUTOMATIC,OS_APPL_CODE) waiter);

#endif /* OS_ALARM_INTERNAL_H */
