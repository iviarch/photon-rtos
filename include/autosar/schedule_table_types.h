/*
 * PhotonRTOS础光实时操作系统 -- AUTOSAR调度表类型
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Lin Yang <s-yanglin@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef OS_SCHED_TABLE_TYPES_H
#define OS_SCHED_TABLE_TYPES_H

#include <autosar/common_types.h>

typedef VAR(uint8, TYPEDEF) ScheduleTableType;
typedef VAR(uint8, TYPEDEF) ScheduleTableStatusType;
typedef P2VAR(ScheduleTableStatusType, TYPEDEF, OS_APPL_DATA)		\
	ScheduleTableStatusRefType;

/*
 * 调度表状态码：
 *      SCHEDULETABLE_STOPPED：调度表未启动
 *      SCHEDULETABLE_NEXT：调度表将在当前运行的调度表结束后启动（调度表在
 *		                    NextScheduleTable() 服务中使用
 *      SCHEDULETABLE_WAITING：schedule 表使用显式同步，
 *		                    已启动并正在等待全局时间
 *      SCHEDULETABLE_RUNNING：调度表正在运行，但当前与全局时间源不同步
 *      SCHEDULETABLE_RUNNING_AND_SYNCHRONOUS：调度表正在运行并且与全局时间源同步
 */
enum {
		__SCHEDULETABLE_STOPPED = 0,
		__SCHEDULETABLE_NEXT,
		__SCHEDULETABLE_WAITING,
		__SCHEDULETABLE_RUNNING,
		__SCHEDULETABLE_RUNNING_AND_SYNCHRONOUS,
		__SCHEDULETABLE_MAX,
};

#define SCHEDULETABLE_STOPPED   ((ScheduleTableStatusType)__SCHEDULETABLE_STOPPED)
#define SCHEDULETABLE_NEXT	((ScheduleTableStatusType)__SCHEDULETABLE_NEXT)
#define SCHEDULETABLE_WAITING	((ScheduleTableStatusType)__SCHEDULETABLE_WAITING)
#define SCHEDULETABLE_RUNNING	((ScheduleTableStatusType)__SCHEDULETABLE_RUNNING)
#define SCHEDULETABLE_RUNNING_AND_SYNCHRONOUS				\
    ((ScheduleTableStatusType)__SCHEDULETABLE_RUNNING_AND_SYNCHRONOUS)

#endif /* OS_SCHED_TABLE_TYPES_H */
