/*
 * PhotonRTOS础光实时操作系统 -- AUTOSAR多核内部实现
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Zicheng Hu <huzicheng@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef OS_MULTICORE_INTERNAL_H
#define OS_MULTICORE_INTERNAL_H

#include <autosar/multicore.h>
#include <autosar/system.h>
#include <photon/cpu.h>
#include <photon/cpumask.h>
#include <photon/smp.h>
#include <autosar/calllevel_internal.h>

/**
 * static check max cpus and non autosar cores
 */
#if OsNumberOfNonAutosarCores > (OsNumberOfCores - 1)
#error "NON_AUTOSAR_CORES must less than MAX_CPUS"
#endif

#define AUTOSAR_CORES	(OsNumberOfCores - OsNumberOfNonAutosarCores)

enum CoreStatue {
	UNKNOWN_CORE = 0,
	AUTOSAR_CORE,
	NONE_AUTOSAR_CORE,
};

typedef VAR(enum CoreStatue, TYPEDEF) CoreStatueType;

extern VAR(uint32, OS_VAR_INIT) autosar_activeated_cores;

extern VAR(cpumask_t, OS_VAR_INIT) autosar_startos_core;

FUNC(void, OS_CODE) autosar_multicore_preinit(void);
FUNC(CoreIdType, OS_CODE) autosar_get_core_id(void);
FUNC(CoreStatueType, OS_CODE) autosar_get_core_status(VAR(CoreIdType, AUTOMATIC) CoreID);
FUNC(void, OS_CODE) autosar_start_core(VAR(CoreIdType, AUTOMATIC) CoreID);
FUNC(void, OS_CODE) non_autosar_start_core(VAR(CoreIdType, AUTOMATIC) CoreID);

FUNC(void, OS_CODE) autosar_set_idle_mode(VAR(CoreIdType, AUTOMATIC) CoreID,
	VAR(IdleModeType, AUTOMATIC) IdleMode);
FUNC(IdleModeType, OS_CODE) autosar_get_idle_mode(VAR(CoreIdType, AUTOMATIC) CoreID);

FUNC(void, OS_CODE) autosar_set_app_mode(VAR(CoreIdType, AUTOMATIC) CoreID,
	VAR(AppModeType, AUTOMATIC) AppMode);
FUNC(AppModeType, OS_CODE) autosar_get_app_mode(VAR(CoreIdType, AUTOMATIC) CoreID);

FUNC(void, OS_CODE) autosar_sync_all_cores(P2VAR(uintptr_t, AUTOMATIC,
	OS_APPL_DATA) wait_data, int32_t cores);
FUNC(void, OS_CODE) autosar_shutdown_core(void);
FUNC(void, OS_CODE) autosar_shutdown_os(VAR(StatusType, AUTOMATIC) Error);

#endif /* OS_MULTICORE_INTERNAL_H */
