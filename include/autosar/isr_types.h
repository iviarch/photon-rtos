/*
 * PhotonRTOS础光实时操作系统 -- AUTOSAR中断类型
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Yili Zhang <s-zhangyili@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef OS_ISR_TYPES_H
#define OS_ISR_TYPES_H

#include <autosar/common_types.h>

/*ISR2中断*/
#define INVALID_ISR (-1)

typedef VAR(int32, TYPEDEF) ISRType;

/**
 * ISR宏定义，定义一个ISR驱动函数
 *        name：任务名称
 */
#define ISR(Name)						\
		extern void OS_ISR_##Name(void);	\
		void OS_ISR_##Name(void)

#endif /* OS_ISR_TYPES_H */
