/*
 * PhotonRTOS础光实时操作系统 -- AUTOSAR外设接口
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Yili Zhang <s-zhangyili@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef OS_PERIPHERAL_H
#define OS_PERIPHERAL_H

#include <autosar/peripheral_types.h>

/*
 * 语法
 * StatusType ReadPeripheral8 (
 *       AreaIdType Area,
 *       const uint8* Address,
 *       uint8* ReadValue
 * )
 *
 * 服务 ID [hex]
 *      0x28
 *
 * 同步/异步
 *      同步（Synchronous）
 *
 * 可重入性
 *      可重入
 *
 * 参数 (in)
 *      Area 硬件外围区域参考
 *      Address 内存地址
 *
 * 参数 (inout)
 *      None
 *
 * 参数 (out)
 *      Read
 *      Value
 *      给定内存位置的内容（<地址>）
 *
 * 返回值
 *      StatusType
 *      E_OK 无错误
 *      E_OS_ID 区域 ID 超出范围（EXTENDED 状态）
 *      E_OS_VALUE 地址不属于给定区域（扩展状态）
 *      E_OS_CALLEVEL API 函数调用上下文错误（扩展状态）
 *      E_OS_ACCESS 调用任务或 ISR 不允许访问给定的
 *
 * 描述
 *      此服务返回给定内存位置 (<Address>) 的内容。
 *
 * 引用
 *      Os.h
 */
FUNC(StatusType, OS_CODE) ReadPeripheral8(
	VAR(AreaIdType, AUTOMATIC) Area,
	P2CONST(uint8, AUTOMATIC, OS_CONST) Address,
	P2VAR(uint8, TYPEDEF, OS_APPL_DATA) ReadValue);

/*
 * 语法
 * StatusType ReadPeripheral16 (
 *       AreaIdType Area,
 *       const uint8* Address,
 *       uint8* ReadValue
 * )
 *
 * 服务 ID [hex]
 *      0x28
 *
 * 同步/异步
 *      同步（Synchronous）
 *
 * 可重入性
 *      可重入
 *
 * 参数 (in)
 *      Area 硬件外围区域参考
 *      Address 内存地址
 *
 * 参数 (inout)
 *      None
 *
 * 参数 (out)
 *      Read
 *      Value
 *      给定内存位置的内容（<地址>）
 *
 * 返回值
 *      StatusType
 *      E_OK 无错误
 *      E_OS_ID 区域 ID 超出范围（EXTENDED 状态）
 *      E_OS_VALUE 地址不属于给定区域（扩展状态）
 *      E_OS_CALLEVEL API 函数调用上下文错误（扩展状态）
 *      E_OS_ACCESS 调用任务或 ISR 不允许访问给定的
 *
 * 描述
 *      此服务返回给定内存位置 (<Address>) 的内容。
 *
 * 引用
 *      Os.h
 */
FUNC(StatusType, OS_CODE) ReadPeripheral16(
	VAR(AreaIdType, AUTOMATIC) Area,
	P2CONST(uint16, AUTOMATIC, OS_CONST) Address,
	P2VAR(uint16, TYPEDEF, OS_APPL_DATA) ReadValue);

/*
 * 语法
 * StatusType ReadPeripheral32 (
 *       AreaIdType Area,
 *       const uint32* Address,
 *       uint32* ReadValue
 * )
 *
 * 服务 ID [hex]
 *      0x28
 *
 * 同步/异步
 *      同步（Synchronous）
 *
 * 可重入性
 *      可重入
 *
 * 参数 (in)
 *      Area 硬件外围区域参考
 *      Address 内存地址
 *
 * 参数 (inout)
 *      None
 *
 * 参数 (out)
 *      Read
 *      Value
 *      给定内存位置的内容（<地址>）
 *
 * 返回值
 *      StatusType
 *      E_OK 无错误
 *      E_OS_ID 区域 ID 超出范围（EXTENDED 状态）
 *      E_OS_VALUE 地址不属于给定区域（扩展状态）
 *      E_OS_CALLEVEL API 函数调用上下文错误（扩展状态）
 *      E_OS_ACCESS 调用任务或 ISR 不允许访问给定的
 *
 * 描述
 *      此服务返回给定内存位置 (<Address>) 的内容。
 *
 * 引用
 *      Os.h
 */
FUNC(StatusType, OS_CODE) ReadPeripheral32(
	VAR(AreaIdType, AUTOMATIC) Area,
	P2CONST(uint32, AUTOMATIC, OS_CONST) Address,
	P2VAR(uint32, TYPEDEF, OS_APPL_DATA) ReadValue);

/*
 * 语法
 *   StatusType WritePeripheral8 (
 *      AreaIdType Area,
 *      uint8* Address,
 *      uint8 WriteValue
 *  )
 *
 * 服务 ID [hex]
 *      0x28
 *
 * 同步/异步
 *      同步（Synchronous）
 *
 * 可重入性
 *      可重入
 *
 * 参数 (in)
 *      Area 硬件外围区域参考
 *      Address 内存地址
 *
 * 参数 (inout)
 *      None
 *
 * 参数 (out)
 *      Read
 *      Value
 *      给定内存位置的内容（<地址>）
 *
 * 返回值
 *      StatusType
 *      E_OK 无错误
 *      E_OS_ID 区域 ID 超出范围（EXTENDED 状态）
 *      E_OS_VALUE 地址不属于给定区域（扩展状态）
 *      E_OS_CALLEVEL API 函数调用上下文错误（扩展状态）
 *      E_OS_ACCESS 调用任务或 ISR 不允许访问给定的
 *
 * 描述
 *      该服务将 <value> 写入给定的内存位置（<memory address>）
 *
 * 引用
 *      Os.h
 */


FUNC(StatusType, OS_CODE) WritePeripheral8(
	VAR(AreaIdType, AUTOMATIC) Area,
	P2VAR(uint8, TYPEDEF, OS_APPL_DATA) Address,
	VAR(uint8, AUTOMATIC) WriteValue);

/*
 * 语法
 *   StatusType WritePeripheral16 (
 *      AreaIdType Area,
 *      uint16* Address,
 *      uint16 WriteValue
 *  )
 *
 * 服务 ID [hex]
 *      0x28
 *
 * 同步/异步
 *      同步（Synchronous）
 *
 * 可重入性
 *      可重入
 *
 * 参数 (in)
 *      Area 硬件外围区域参考
 *      Address 内存地址
 *
 * 参数 (inout)
 *      None
 *
 * 参数 (out)
 *      Read
 *      Value
 *      给定内存位置的内容（<地址>）
 *
 * 返回值
 *      StatusType
 *      E_OK 无错误
 *      E_OS_ID 区域 ID 超出范围（EXTENDED 状态）
 *      E_OS_VALUE 地址不属于给定区域（扩展状态）
 *      E_OS_CALLEVEL API 函数调用上下文错误（扩展状态）
 *      E_OS_ACCESS 调用任务或 ISR 不允许访问给定的
 *
 * 描述
 *      该服务将 <value> 写入给定的内存位置（<memory address>）
 *
 * 引用
 *      Os.h
 */


FUNC(StatusType, OS_CODE) WritePeripheral16(
	VAR(AreaIdType, AUTOMATIC) Area,
	P2VAR(uint16, TYPEDEF, OS_APPL_DATA) Address,
	VAR(uint16, AUTOMATIC) WriteValue);

/*
 * 语法
 *   StatusType WritePeripheral32 (
 *      AreaIdType Area,
 *      uint32* Address,
 *      uint32 WriteValue
 *  )
 *
 * 服务 ID [hex]
 *      0x28
 *
 * 同步/异步
 *      同步（Synchronous）
 *
 * 可重入性
 *      可重入
 *
 * 参数 (in)
 *      Area 硬件外围区域参考
 *      Address 内存地址
 *
 * 参数 (inout)
 *      None
 *
 * 参数 (out)
 *      Read
 *      Value
 *      给定内存位置的内容（<地址>）
 *
 * 返回值
 *      StatusType
 *      E_OK 无错误
 *      E_OS_ID 区域 ID 超出范围（EXTENDED 状态）
 *      E_OS_VALUE 地址不属于给定区域（扩展状态）
 *      E_OS_CALLEVEL API 函数调用上下文错误（扩展状态）
 *      E_OS_ACCESS 调用任务或 ISR 不允许访问给定的
 *
 * 描述
 *      该服务将 <value> 写入给定的内存位置（<memory address>）
 *
 * 引用
 *      Os.h
 */


FUNC(StatusType, OS_CODE) WritePeripheral32(
	VAR(AreaIdType, AUTOMATIC) Area,
	P2VAR(uint32, TYPEDEF, OS_APPL_DATA) Address,
	VAR(uint32, AUTOMATIC) WriteValue);

/*
 * 语法
 *   StatusType ModifyPeripheral8 (
 *       AreaIdType Area,
 *       uint8* Address,
 *       uint8 Clearmask,
 *       uint8 Setmask
 *)
 *
 * 服务 ID [hex]
 *      0x28
 *
 * 同步/异步
 *      同步（Synchronous）
 *
 * 可重入性
 *      可重入
 *
 * 参数 (in)
 *      Area 硬件外围区域参考
 *      Address 内存地址
 *      Clearmask 内存地址将由位与修改
 *       Setmask 内存地址将由位或修改
 *
 * 参数 (inout)
 *      None
 *
 * 参数 (out)
 *      None
 *
 * 返回值
 *      StatusType
 *      E_OK 无错误
 *      E_OS_ID 区域 ID 超出范围（EXTENDED 状态）
 *      E_OS_VALUE 地址不属于给定区域（扩展状态）
 *      E_OS_CALLEVEL API 函数调用上下文错误（扩展状态）
 *      E_OS_ACCESS 调用任务或 ISR 不允许访问给定的
 *
 * 描述
 *      此服务使用以下公式修改给定的内存位置 (<memory address>)：*<Address> =
 *  ((*<Address> & <clearmask>) | <setmask>)
 *
 * 引用
 *      Os.h
 */
FUNC(StatusType, OS_CODE) ModifyPeripheral8(
	VAR(AreaIdType, AUTOMATIC) Area,
	P2VAR(uint8, TYPEDEF, OS_APPL_DATA) Address,
	VAR(uint8, AUTOMATIC) Clearmask,
	VAR(uint8, AUTOMATIC) Setmask);

/*
 * 语法
 *   StatusType ModifyPeripheral16 (
 *       AreaIdType Area,
 *       uint16* Address,
 *       uint16 Clearmask,
 *       uint16 Setmask
 *)
 *
 * 服务 ID [hex]
 *      0x28
 *
 * 同步/异步
 *      同步（Synchronous）
 *
 * 可重入性
 *      可重入
 *
 * 参数 (in)
 *      Area 硬件外围区域参考
 *      Address 内存地址
 *      Clearmask 内存地址将由位与修改
 *       Setmask 内存地址将由位或修改
 *
 * 参数 (inout)
 *      None
 *
 * 参数 (out)
 *      None
 *
 * 返回值
 *      StatusType
 *      E_OK 无错误
 *      E_OS_ID 区域 ID 超出范围（EXTENDED 状态）
 *      E_OS_VALUE 地址不属于给定区域（扩展状态）
 *      E_OS_CALLEVEL API 函数调用上下文错误（扩展状态）
 *      E_OS_ACCESS 调用任务或 ISR 不允许访问给定的
 *
 * 描述
 *      此服务使用以下公式修改给定的内存位置 (<memory address>)：*<Address> =
 *  ((*<Address> & <clearmask>) | <setmask>)
 *
 * 引用
 *      Os.h
 */


FUNC(StatusType, OS_CODE) ModifyPeripheral16(
	VAR(AreaIdType, AUTOMATIC) Area,
	P2VAR(uint16, TYPEDEF, OS_APPL_DATA) Address,
	VAR(uint16, AUTOMATIC) Clearmask,
	VAR(uint16, AUTOMATIC) Setmask);

/*
 * 语法
 *   StatusType ModifyPeripheral32 (
 *       AreaIdType Area,
 *       uint32* Address,
 *       uint32 Clearmask,
 *       uint32 Setmask
 *)
 *
 * 服务 ID [hex]
 *      0x28
 *
 * 同步/异步
 *      同步（Synchronous）
 *
 * 可重入性
 *      可重入
 *
 * 参数 (in)
 *      Area 硬件外围区域参考
 *      Address 内存地址
 *      Clearmask 内存地址将由位与修改
 *       Setmask 内存地址将由位或修改
 *
 * 参数 (inout)
 *      None
 *
 * 参数 (out)
 *      None
 *
 * 返回值
 *      StatusType
 *      E_OK 无错误
 *      E_OS_ID 区域 ID 超出范围（EXTENDED 状态）
 *      E_OS_VALUE 地址不属于给定区域（扩展状态）
 *      E_OS_CALLEVEL API 函数调用上下文错误（扩展状态）
 *      E_OS_ACCESS 调用任务或 ISR 不允许访问给定的
 *
 * 描述
 *      此服务使用以下公式修改给定的内存位置 (<memory address>)：*<Address> =
 *  ((*<Address> & <clearmask>) | <setmask>)
 *
 * 引用
 *      Os.h
 */


FUNC(StatusType, OS_CODE) ModifyPeripheral32(
	VAR(AreaIdType, AUTOMATIC) Area,
	P2VAR(uint32, TYPEDEF, OS_APPL_DATA) Address,
	VAR(uint32, AUTOMATIC) Clearmask,
	VAR(uint32, AUTOMATIC) Setmask);

#endif /* OS_PERIPHERAL_H */
