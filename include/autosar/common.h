/*
 * PhotonRTOS础光实时操作系统 -- AUTOSAR通用头文件
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Zicheng Hu <huzicheng@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef OS_AUTOSAR_COMMON_H
#define OS_AUTOSAR_COMMON_H

#include <autosar/common_types.h>
#include <photon/double_list.h>
#include <photon/smp_lock.h>

typedef VAR(struct double_list, TYPEDEF) OsListType;
typedef P2VAR(OsListType, AUTOMATIC, OS_APPL_DATA) OsListRefType;
typedef VAR(struct smp_lock, TYPEDEF) OsSpinLockType;

FUNC(void, OS_CODE) main(void);

#define AUTOSTART(v)        (v)
#define PRIORITY(v)         (v)
#define TASK_FLAG(v)        (v)
#define APP_MODE(v)         (v)
#define CORE_ID(v)          (v)
/**
 * 两个task 被激活的最小时间间隔.
 */
#define TASK_TIME_FRAME(v)  (v)
/**
 * 两次ISR发生的最小时间间隔.
 */
#define ISR_TIME_FRAME(v)   (v)
/**
 * 一次ISR持续的最大时间.
 */
#define ISR_TIME_BUDGET(v)  (v)
/**
 * 一个TASK持续的最大时间
 */
#define TASK_TIME_BUDGET(v) (v)
#define APP_ID(v)           (v)
#define SchedPolicy(v)		(v)

#define MAX_VALUE(v)        (v)
#define TICKS_PER_BASE(v)   (v)
#define MIN_CYCLES(v)       (v)


#endif /* __CHUSHI_AUTOSAR_COMMON_H */

