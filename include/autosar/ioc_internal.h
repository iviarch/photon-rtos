/*
 * 础光实时操作系统PhotonRTOS -- IOC通讯接口
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Rui Yang <yangrui@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef IOC_INTERNAL_H
#define IOC_INTERNAL_H

#include <autosar/internal.h>
#include <photon/ring_queue.h>
#include <autosar/ioc_config.h>

#define get_array_count(x) sizeof(x)/sizeof(x[0])

typedef enum{
    IOC_E_OK,
    IOC_E_NOK,
    IOC_E_LIMIT,
    IOC_E_LOST_DATA,
    IOC_E_NO_DATA,
    IOC_E_LENGTH,
} Std_ReturnType;

struct Element {
    P2VAR(uint8_t, TYPEDEF, OS_APPL_DATA) PData;
    VAR(uint16_t, TYPEDEF) DataSize;
};

struct IocData {
    VAR(uint16, TYPEDEF) IocId;
    VAR(uint16, TYPEDEF) SenderId;
    VAR(uint16, TYPEDEF) ReceiverId;
    VAR(uint16, TYPEDEF) ElementNum;
    VAR(struct Element, TYPEDEF) Element[20];    
};

struct InnerQueueGroup
{
    VAR(uint32_t, TYPEDEF) RecCoreId;
    P2FUNC(void, TYPEDEF, FunctionCb)(void);
    VAR(struct accurate_counter, TYPEDEF) IpiCount;
    VAR(struct ring, TYPEDEF) RingInfo;
};

FUNC(void, OS_CODE) IocInit(void);
FUNC(void, OS_CODE) IocEmptyQueue(VAR(uint16, AUTOMATIC) IocId, VAR(bool, AUTOMATIC) group);
FUNC(Std_ReturnType, OS_CODE) IocSend(P2CONST(struct IocData, AUTOMATIC, OS_CONST) data);
FUNC(Std_ReturnType, OS_CODE) IocReceive(P2CONST(struct IocData, AUTOMATIC, OS_CONST) data);
FUNC(Std_ReturnType, OS_CODE) IocWrite(P2CONST(struct IocData, AUTOMATIC, OS_CONST) data);
FUNC(Std_ReturnType, OS_CODE) IocRead(P2CONST(struct IocData, AUTOMATIC, OS_CONST) data);

#endif


