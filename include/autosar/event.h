/*
 * PhotonRTOS础光实时操作系统 -- AUTOSAR事件接口
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Baoyou Xie <xiebaoyou@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef OS_EVENT_H
#define OS_EVENT_H

#include <autosar/event_types.h>
#include <autosar/task_types.h>

/**
 * 调用ClearEvent的扩展任务的事件被清除
 * 这些事件与事件掩码<Mask>相关。
 * 语法：
 *     StatusType ClearEvent ( EventMaskType <Mask> )
 * 参数（输入）：
 *     Mask			要清除的事件掩码
 *  参数（输出）：
 *    无
 * 特殊说明：
 *    系统服务ClearEvent限制为拥有事件的扩展任务。
 * 状态：
 *    标准：
 *        无错误，E_OK
 *    扩展：
 *        调用任务不是扩展任务，E_OS_ACCESS
 *        在中断级调用，E_OS+CALLEVEL
 * 一致性：
 *     ECC1, ECC2
 */
extern FUNC(StatusType, OS_CODE) ClearEvent(
	VAR(EventMaskType, AUTOMATIC) Mask);

/**
 * 该服务返回任务<TaskID>所有事件位的当前状态，
 * 而不是任务当前正在等待的事件。
 * 该服务可以从中断服务例程、
 * 任务级以及某些钩子例程中调用。
 * 语法：
 *     StatusType GetEvent ( TaskType <TaskID>, EventMaskRefType <Event> )
 *  参数（输入）：
 *     TaskID		 要从其中返回事件掩码的任务
 * 参数（输出）：
 *     Event			 返回数据的内存引用
 * 特殊说明：
 *     引用的任务应当是扩展任务。
 * 状态：
 *     标准：
 *        无错误，E_OK
 *     扩展：
 *        任务<TaskID>无效，E_OS_ID
 *        调用任务不是扩展任务，E_OS_ACCESS
 *        引用的任务<TaskID>处于挂起状态，E_OS_STATE
 * 一致性：
 *     ECC1, ECC2
 */
extern FUNC(StatusType, OS_CODE) GetEvent(
	VAR(TaskType, AUTOMATIC) TaskID,
	VAR(EventMaskRefType, AUTOMATIC) Event);

/**
 *  该服务可以从中断服务例程和任务级调用，
 * 但是不能从钩子例程中调用。
 *  任务<TaskID>的事件是与事件掩码<Mask>相关的集合。
 * 如果任务正在等待<Mask>指定的事件之一，
 * 那么调用SetEvent导致任务<TaskID>被转换为就绪状态。
 * 语法：
 *     StatusType SetEvent ( TaskType <TaskID>
 *          EventMaskType <Mask> )
 * 参数（输入）：
 *     TaskID	将要对其设置一个或者几个事件的任务引用
 *     Mask		要设置的事件掩码
 * 参数（输出）：
 *     无
 * 特殊说明：
 *     未在事件掩码中设置的任何事件保持不变。
 * 状态：
 *     标准：
 *        无错误，E_OK
 *     扩展：
 *        任务<TaskID>无效，E_OS_ID
 *        引用的任务不是扩展任务，E_OS_ACCESS
 *        由于引用任务处于挂起状态，
 *        事件不能被设置，E_OS_STATE
 * 一致性：
 *     ECC1, ECC2
 */
extern FUNC(StatusType, OS_CODE) SetEvent(
	VAR(TaskType, AUTOMATIC) TaskID,
	VAR(EventMaskType, AUTOMATIC) Mask);

/**
 * 调用任务的状态被设置为等待
 * 除非在<Mask>中指定的至少一个事件已经被设置。
 * 语法：
 *     StatusType WaitEvent ( EventMaskType <Mask> )
 * 参数（输入）：
 *     Mask			要等待的事件掩码
 * 参数（输出）：
 *     无
 * 特殊说明：
 *     如果等待条件发生，该服务确保产生重调度。
 *     如果产生重调度，在任务处于等待状态时，
 *     任务的内部资源被释放。
 *     该服务仅仅应当在拥有事件的扩展任务中调用。
 * 状态：
 *     标准：
 *        无错误，E_OK
 *     扩展：
 *        调用任务不是扩展任务，E_OS_ACCESS
 *        调用任务占据资源。E_OS_RESOURCE
 *        在中断级调用，E_OS_CALLEVEL
 * 一致性：
 *     ECC1, ECC2
 */
extern FUNC(StatusType, OS_CODE) WaitEvent(
	VAR(EventMaskType, AUTOMATIC) Mask);

#if defined(AUTOSAR_OS_APPLICATION)
/*
 * 语法
 * void SetEventAsyn (
 *      TaskType id,
 *      EventMaskType m
 * )
 *
 * 服务 ID [hex]
 *      0x34
 *
 * 同步/异步
 *      异步（Asynchronous ）
 *
 * 可重入性
 *      可重入
 *
 * 参数 (in)
 *      id
 *      待激活任务的id
 *      m
 *      要设置的事件的掩码
 *
 * 参数 (inout)
 *      None
 *
 * 参数 (out)
 *      None
 *
 * 返回值
 *      None
 *
 * 描述
 *      SetEvent() 函数的异步版本。旨在用于跨核心事件设置。可能的错误不
 * 会返回给调用者，但可能会通过错误钩子报告。
 *
 * 引用
 *      AUTOSAR OS 8.4.39 SetEventAsyn [SWS_Os_91023]
 */
FUNC(void, OS_CODE) SetEventAsyn(
	VAR(TaskType, AUTOMATIC) id,
	VAR(EventMaskType, AUTOMATIC) m);
#endif /* defined(AUTOSAR_OS_APPLICATION) */

#endif /* OS_EVENT_H */
