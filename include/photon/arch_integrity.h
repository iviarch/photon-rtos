/*
 * PhotonRTOS础光实时操作系统 -- 架构函数定义完整性检查头文件
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Lin Yang <s-yanglin@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef _OS_ARCG_INTEGRITY_H
#define _OS_ARCG_INTEGRITY_H


#include <asm/accurate_counter.h>

#ifndef ARCH_FUN_arch_accurate_add_DEFINED
#error arch function "arch_accurate_add" not defined
#endif

#ifndef ARCH_FUN_arch_accurate_sub_DEFINED
#error arch function "arch_accurate_sub" not defined
#endif

#ifndef ARCH_FUN_arch_accurate_cmpxchg_DEFINED
#error arch function "arch_accurate_cmpxchg" not defined
#endif

#ifndef ARCH_FUN_arch_accurate_xchg_DEFINED
#error arch function "arch_accurate_xchg" not defined
#endif


#include <asm/arch_timer.h>

#ifndef ARCH_FUN_arch_timer_get_cntfrq_DEFINED
#error arch function "arch_timer_get_cntfrq" not defined
#endif


#include <asm/bitops.h>

#ifndef ARCH_FUN_atomic_set_bit_DEFINED
#error arch function "atomic_set_bit" not defined
#endif

#ifndef ARCH_FUN_atomic_clear_bit_DEFINED
#error arch function "atomic_clear_bit" not defined
#endif


#include <asm/internel.h>

#ifndef ARCH_FUN_arch_switch_cpu_context_DEFINED
#error arch function "arch_switch_cpu_context" not defined
#endif

#ifndef ARCH_FUN_start_arch_DEFINED
#error arch function "start_arch" not defined
#endif

#ifndef ARCH_FUN_init_time_DEFINED
#error arch function "init_time" not defined
#endif

#ifndef ARCH_FUN_dump_task_stack_DEFINED
#error arch function "dump_task_stack" not defined
#endif

#ifndef ARCH_FUN_arch_launch_cpu_DEFINED
#error arch function "arch_launch_cpu" not defined
#endif


#include <asm/io.h>

#ifndef readl_relaxed
#error arch function "readl_relaxed" not defined
#endif

#ifndef writel_relaxed
#error arch function "writel_relaxed" not defined
#endif

#ifndef readl
#error arch function "readl" not defined
#endif

#ifndef writel
#error arch function "writel" not defined
#endif


#include <asm/irq.h>

#ifndef ARCH_FUN_set_chip_irq_handle_DEFINED
#error arch function "set_chip_irq_handle" not defined
#endif


#include <asm/irqflags.h>

#ifndef ARCH_FUN_arch_local_irq_save_DEFINED
#error arch function "arch_local_irq_save" not defined
#endif

#ifndef ARCH_FUN_arch_local_irq_enable_DEFINED
#error arch function "arch_local_irq_enable" not defined
#endif

#ifndef ARCH_FUN_arch_local_irq_disable_DEFINED
#error arch function "arch_local_irq_disable" not defined
#endif

#ifndef ARCH_FUN_arch_local_save_flags_DEFINED
#error arch function "arch_local_save_flags" not defined
#endif

#ifndef ARCH_FUN_arch_local_irq_restore_DEFINED
#error arch function "arch_local_irq_restore" not defined
#endif

#ifndef ARCH_FUN_arch_irqs_disabled_flags_DEFINED
#error arch function "arch_irqs_disabled_flags" not defined
#endif

#ifndef ARCH_FUN_arch_local_isr_save_DEFINED
#error  arch function "arch_local_isr_save" not defined
#endif

#ifndef ARCH_FUN_arch_local_isr_restore_DEFINED
#error  arch function "arch_local_isr_restore" not defined
#endif

#include <asm/process.h>

#ifndef ARCH_FUN_current_proc_info_DEFINED
#error arch function "current_proc_info" not defined
#endif


#include <asm/processor.h>

#ifndef ARCH_FUN_cpu_do_idle_DEFINED
#error arch function "cpu_do_idle" not defined
#endif


#include <asm/smp_lock.h>

#ifndef ARCH_FUN_arch_smp_lock_value_unlocked_DEFINED
#error arch function "arch_smp_lock_value_unlocked" not defined
#endif

#ifndef ARCH_FUN_arch_smp_lock_is_locked_p_DEFINED
#error arch function "arch_smp_lock_is_locked_p" not defined
#endif

#ifndef ARCH_FUN_arch_smp_lock_DEFINED
#error arch function "arch_smp_lock" not defined
#endif

#ifndef ARCH_FUN_arch_smp_trylock_DEFINED
#error arch function "arch_smp_trylock" not defined
#endif

#ifndef ARCH_FUN_arch_smp_unlock_DEFINED
#error arch function "arch_smp_unlock" not defined
#endif


#include <asm/smp.h>

#ifndef ARCH_FUN_smp_processor_id_DEFINED
#error arch function "smp_processor_id" not defined
#endif


#include <asm/timex.h>

#ifndef ARCH_FUN_get_cycles_DEFINED
#error arch function "get_cycles" not defined
#endif

#endif /* _OS_ARCG_INTEGRITY_H */
