/*
 * PhotonRTOS础光实时操作系统 -- 内核头文件
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Baoyou Xie <xiebaoyou@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef __OS_KERNEL_H
#define __OS_KERNEL_H

#include <stdarg.h>
#include <photon/types.h>
#include <photon/bitops.h>
#include <photon/typecheck.h>
#include <photon/printk.h>

#ifndef asmlinkage
#define asmlinkage
#endif
/*
 * 'kernel.h' contains some often-used function prototypes etc
 */
#define __ALIGN_KERNEL(x, a)	\
		__ALIGN_KERNEL_MASK(x,	(typeof(x))(a) - 1)
#define __ALIGN_KERNEL_MASK(x, mask)	(((x) + (mask)) & ~(mask))

#define USHRT_MAX	((uint16_t)(~0U))
#define SHRT_MAX	((int16_t)(USHRT_MAX>>1))
#define INT_MAX		((int32_t)(~0U>>1))

#define ALIGN(x, a)		__ALIGN_KERNEL((x), (a))

#define ARRAY_SIZE(arr) (sizeof(arr) / sizeof((arr)[0]))

#define DIV_ROUND_UP(n, d) (((n) + (d) - 1) / (d))

#undef offsetof
#ifdef __compiler_offsetof
#define offsetof(TYPE, MEMBER) __compiler_offsetof(TYPE, MEMBER)
#else
#define offsetof(TYPE, MEMBER) ((size_t) &((TYPE *)0)->MEMBER)
#endif

/**
 * container_of - cast a member of a structure out to the containing structure
 * @ptr:	the pointer to the member.
 * @type:	the type of the container struct this is embedded in.
 * @member:	the name of the member within the struct.
 *
 */
#ifndef container_of
#define container_of(ptr, type, member) ({			\
	const typeof(((type *)0)->member) * __mptr = (ptr);	\
	(type *)((int8_t *)__mptr - offsetof(type, member)); })
#endif

#define min(x, y) ({				\
	typeof(x) _min1 = (x);			\
	typeof(y) _min2 = (y);			\
	(void) (&_min1 == &_min2);		\
	_min1 < _min2 ? _min1 : _min2; })

#define max(x, y) ({				\
	typeof(x) _max1 = (x);			\
	typeof(y) _max2 = (y);			\
	(void) (&_max1 == &_max2);		\
	_max1 > _max2 ? _max1 : _max2; })

#endif /* __OS_KERNEL_H */
