/*
 * PhotonRTOS础光实时操作系统 -- section地址
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Baoyou Xie <xiebaoyou@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef _ASM_GENERIC_SECTIONS_H_
#define _ASM_GENERIC_SECTIONS_H_

#include <photon/types.h>

extern char __app0_data_start[];
extern char __app0_data_end[];

extern char __app1_data_start[];
extern char __app1_data_end[];

extern char __app0_code_start[];
extern char __app0_code_end[];

extern char __app1_code_start[];
extern char __app1_code_end[];

extern char __rodata_start[];
extern char __rodata_end[];

extern char __CSA0[];
extern char __CSA0_END[];

extern char __CSA1[];
extern char __CSA1_END[];

extern char CPU0_text_start[];
extern char CPU0_text_end[];

typedef struct code_range {
	uintptr_t start;
	uintptr_t end;
	int8_t flag;
}code_range_t;

typedef struct data_range {
	uintptr_t start;
	uintptr_t end;
	int8_t flag;
}data_range_t;

struct app_address_range {
	data_range_t data_range;
};

struct task_address_range {
	code_range_t code_range;
	data_range_t data_range;
};

/**
 * 符号表解析使用
 * 调试
 */
extern char kernel_text_start[];
extern char kernel_text_end[];

extern char kernel_data_start[];
extern char kernel_data_end[];

extern char _etext[];
extern char __bss_start[], __bss_stop[];
extern char __init_begin[], __init_end[];
extern char _sinittext[], _einittext[];

#ifndef arch_is_kernel_text
static inline int32_t arch_is_kernel_text(uintptr_t addr)
{
	return 0;
}
#endif

#endif /* _ASM_GENERIC_SECTIONS_H_ */
