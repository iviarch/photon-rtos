/*
 * PhotonRTOS础光实时操作系统 -- 中断头文件
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Baoyou Xie <xiebaoyou@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef _OS_IRQFLAGS_H
#define _OS_IRQFLAGS_H

#include <photon/typecheck.h>
#include <asm/irqflags.h>

static inline uintptr_t arch_irqs_disabled(void)
{
	return arch_irqs_disabled_flags(arch_local_save_flags());
}

#define disable_irq()		arch_local_irq_disable()
#define enable_irq()		arch_local_irq_enable()
#define local_irq_save(flags)				\
	do {						\
		typecheck(uintptr_t, flags);		\
		flags = arch_local_irq_save();		\
	} while (false)
#define local_irq_restore(flags)			\
	do {						\
		typecheck(uintptr_t, flags);		\
		arch_local_irq_restore(flags);		\
	} while (false)
#define local_save_flags(flags)				\
	do {						\
		typecheck(uintptr_t, flags);		\
		flags = arch_local_save_flags();	\
	} while (false)
#define irqs_disabled_flags(flags)			\
	({						\
		typecheck(uintptr_t, flags);		\
		arch_irqs_disabled_flags(flags);	\
	})
#define irqs_disabled()		(arch_irqs_disabled())

/**
 * 二类中断相关：保存中断状态，启用/关闭
*/
#define local_isr_save(flags)				\
	do {						\
		typecheck(uintptr_t, flags);		\
		flags = arch_local_isr_save();		\
	} while (false)
#define local_isr_restore(flags)			\
	do {						\
		typecheck(uintptr_t, flags);		\
		arch_local_isr_restore(flags);		\
	} while (false)
#endif /* _OS_IRQFLAGS_H */
