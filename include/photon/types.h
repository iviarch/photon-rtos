/*
 * PhotonRTOS础光实时操作系统 -- 类型说明
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Baoyou Xie <xiebaoyou@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef __ASM_TYPES_H
#define __ASM_TYPES_H


#ifndef __ASSEMBLY__

#define DECLARE_BITMAP(name, bits) \
	unsigned long name[BITS_TO_LONGS(bits)]

typedef	signed char		int8_t;
typedef	signed short		int16_t;
typedef	signed int		int32_t;
typedef	long			int32_64_t;
typedef	signed long long	int64_t;
typedef	unsigned char		uint8_t;
typedef	unsigned short		uint16_t;
typedef	unsigned int		uint32_t;
typedef	unsigned long long	uint64_t;
typedef	unsigned long		size_t;
typedef	unsigned long		uint32_64_t;
typedef	_Bool			bool;

typedef	uint32_64_t		uintptr_t;
typedef	int32_64_t		intptr_t;

#undef NULL
#define NULL ((void *)0)

#define true	((bool)1)
#define false	((bool)0)

typedef uint32_t phys_addr_t;
typedef phys_addr_t resource_size_t;

struct double_list {
	struct double_list *next, *prev;
	int is_list;
};

struct hash_list_bucket {
	struct double_list head;
};

struct hash_list_node {
	struct double_list node;
};

#endif /* __ASSEMBLY__ */

#endif /* __ASM_TYPES_H */
