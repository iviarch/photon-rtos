/*
 * PhotonRTOS础光实时操作系统 -- 用于kbuild
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Baoyou Xie <xiebaoyou@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef _OS_KBUILD_H
#define _OS_KBUILD_H
/**
 * 编译时使用
 *
 */
#define DEFINE(sym, val)						\
		asm	volatile("\n->" #sym " %0 " #val : : "i" (val))

#define BLANK() asm volatile("\n->" : : )

#define OFFSET(sym, str, mem)						\
	DEFINE(sym, offsetof(struct str, mem))

#define COMMENT(x)							\
	asm volatile("\n->#" x)

#endif /* _OS_KBUILD_H */
