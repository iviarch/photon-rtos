/*
 * PhotonRTOS础光实时操作系统 -- 控制台头文件
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Baoyou Xie <xiebaoyou@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef _OS_CONSOLE_H
#define _OS_CONSOLE_H

#include <photon/types.h>

struct simple_console {
	int8_t	name[16];
	int32_t	(*init)(void);
	void (*write)(const uint8_t *s, uint32_t count);
};

int32_t init_simple_console(void);

#endif /* _OS_CONSOLE_H */
