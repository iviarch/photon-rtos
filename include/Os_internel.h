/*
 * PhotonRTOS础光实时操作系统 -- OS系统内部头文件
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Yili Zhang <s-zhangyili@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef OS_INTERNEL_H
#define OS_INTERNEL_H
/*AUTOSAR内部头*/
#include <autosar/internal.h>
/*内核头*/
#include <photon/printk.h>
#include <photon/process.h>
#include <photon/time.h>
#include <photon/double_list.h>
#include <photon/kernel.h>
#include <photon/sched.h>
#include <photon/irq.h>
#include <photon/irqflags.h>
#include <photon/smp_lock.h>
#include <photon/string.h>
/*GNU头*/
#include <stdarg.h>
/*架构相关*/
#include <asm/sections.h>
#include <asm/cputype.h>
#include <asm/arch_timer.h>

#endif