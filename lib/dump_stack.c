/*
 * 础光实时操作系统PhotonRTOS -- 堆栈回溯实现文件
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Baoyou Xie <xiebaoyou@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#include <photon/kernel.h>
#include <photon/sched.h>

void dump_stack(void)
{
	dump_task_stack(NULL, NULL);
}
