/*
 * 础光实时操作系统PhotonRTOS -- 天花板协议功能测试代码
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Lin Yang <s-yanglin@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#include "../functest.h"

#define IdTestTask1	1
#define IdTestTask2	2
#define IdTestTask3	3
#define IdResource1_3	3

VAR(uint8, AUTOMATIC) ceiling_sequence_right[MAX_SEQUENCE] = {
	IdTestTask1, IdTestTask1, IdTestTask1, IdTestTask1,
	IdTestTask2, IdTestTask2, IdTestTask3, IdTestTask3,
	IdTestTask3, IdTestTask3, IdTestTask3, IdTestTask2,
	IdTestTask1, SEQUENCE_END,
};

TEST(ceiling)
{
	LOGI("天花板协议测试开始\n");
	LOGI("优先级: task1 < task2 < task3\n");
	LOGI("resource1_3 被task1和task3共享使用\n");
	ActivateTask(IdTestTask1);
	ASSERT_SEQUENCE(ceiling_sequence_right);
}


TASK(TestTask1, app0)
{
	SEQUENCE_RECORD("\t|\t\t\t task start");

	SEQUENCE_RECORD("\t|\t\t\t get resource1_3");
	GetResource(IdResource1_3);

	SEQUENCE_RECORD("\t||\t\t\t activate task2");
	ActivateTask(IdTestTask2);

	SEQUENCE_RECORD("\t||\t\t\t release resource1_3");
	ReleaseResource(IdResource1_3);

	SEQUENCE_RECORD("\t|\t\t\t task end");
	TerminateTask();
}

TASK(TestTask2, app0)
{
	SEQUENCE_RECORD("\t\t|\t\t task start");

	SEQUENCE_RECORD("\t\t|\t\t activate task3");
	ActivateTask(IdTestTask3);

	SEQUENCE_RECORD("\t\t|\t\t task end");
	TerminateTask();
}

TASK(TestTask3, app0)
{
	SEQUENCE_RECORD("\t\t\t|\t task start");

	SEQUENCE_RECORD("\t\t\t|\t get resource1_3");
	GetResource(IdResource1_3);

	SEQUENCE_RECORD("\t\t\t||\t");

	SEQUENCE_RECORD("\t\t\t||\t release resource1_3");
	ReleaseResource(IdResource1_3);

	SEQUENCE_RECORD("\t\t\t|\t task end");
	TerminateTask();
}


TEST_SUIT(ceiling)
{
	FUNCTEST_CASE(ceiling);
}

