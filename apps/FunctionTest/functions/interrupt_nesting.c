/*
 * 础光实时操作系统PhotonRTOS -- 中断嵌套功能测试代码
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Yili Zhang <s-zhangyili@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#include "../functest.h"

#ifdef AUTOSAR_INTERRUPT_NESTING_TEST

TEST(interrupt_nesting)
{
	uint32_t tick;
	LOGI("中断嵌套测试开始\n");
	tick = (uint32_t)get_jiffies_64();
	/**
	 * 保持若干个ticks
	*/
	while ((uint32_t)get_jiffies_64() - tick < 10);
	/**
	 * 然后屏蔽掉这些测试中断
	*/
	mask_irq(69);
	mask_irq(70);
	LOGI("中断嵌套测试结束\n");
}
/**
 * 中断嵌套的测试handler，这个优先级高
 */
extern int xilinx_ttc_int_status(int cpu, uint32_t countid);
static void nesting_ttc0_1_handler(void)
{
	/*清除中断源*/
	int cpu = read_cpuid() & 0xff;
	xilinx_ttc_int_status(cpu, 1);
	/*开中断*/
	enable_irq();
	printk("进入高优先级定时器中断\n");
	printk("退出高优先级定时器中断\n");
	/*关中断*/
	disable_irq();
}
/**
 * 这个优先级低
 */
static void nesting_ttc0_2_handler(void)
{
	uint32_t count = 90000;
	/*清除中断源*/
	int cpu = read_cpuid() & 0xff;
	xilinx_ttc_int_status(cpu, 2);
	/*开中断*/
	enable_irq();
	printk("进入低优先级定时器中断\n");
	while (count--);
	printk("退出低优先级定时器中断\n");
	/*关中断*/
	disable_irq();
}

ISR_FUN(cortexr5_timer_ttc0_1);

ISR_FUN(cortexr5_timer_ttc0_1)
{
	nesting_ttc0_1_handler();
}

ISR_FUN(cortexr5_timer_ttc0_2);

ISR_FUN(cortexr5_timer_ttc0_2)
{
	nesting_ttc0_2_handler();
}

#endif

TEST_SUIT(interrupt_nesting)
{
#ifdef AUTOSAR_INTERRUPT_NESTING_TEST
	FUNCTEST_CASE(interrupt_nesting);
#endif
}
