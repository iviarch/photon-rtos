/*
 * 础光实时操作系统PhotonRTOS -- 功能测试
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Lin Yang <s-yanglin@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#include "functest.h"
#include <Os.h>
#include <asm/mpu.h>
#include <photon/smp.h>

extern TEST_SUIT(ceiling);
extern TEST_SUIT(event);
extern TEST_SUIT(counter);
extern TEST_SUIT(scheduletable);
extern TEST_SUIT(interrupt_nesting);

extern char __app0_data_end[];

#define IdCounter 0

#define IdScheduleTable0	0

TASK(FunctionTest, app0)
{
	FUNCTEST_START();
	FUNCTEST_SUITE(ceiling);
	FUNCTEST_SUITE(event);
	FUNCTEST_SUITE(counter);
	FUNCTEST_SUITE(scheduletable);
	FUNCTEST_END();

#if defined(AUTOSAR_SHOW_TASK_INFO)
	ShowTaskInfo(BASE_INFO, ALL_TASK);
	ShowTaskInfo(EXT_INFO, ALL_TASK);
#endif /* defined(AUTOSAR_SHOW_TASK_INFO) */

	ShutdownOS(E_OK);
	TerminateTask();
}

static void print_banner(void)
{
	printk("\n\r"	\
		"  88888888888888       88     88              88      88888888888888\n\r"\
		"  88888888888888   888888 88  88    888888 88 88 88   88888888888888\n\r"\
		"  88          88   888888  88 88    888888 88 88 88       88\n\r"	\
		"  88 88888888 88      88    8 88      88   88 88 88       88\n\r"	\
		"  88 88888888 88   8888888    88      88   88 88 88       88888888\n\r"	\
		"  88    88    88   8888888 8  88     88888 88888888      888888888\n\r"	\
		"  88 88888888 88      88   88 88    888888 88888888     8888    88\n\r"	\
		"  88    88    88     8888   8 88    888 88    88       88 88    88\n\r"	\
		"  88    88  8 88    888888    88     88 88 88 88 88   88  88    88\n\r"	\
		"  88 88888888888   88 88 888888888   88 88 88 88 88       88    88\n\r"	\
		"  88 88888888888   8  88 8888888     88888 88 88 88       88    88\n\r"	\
		"  88          88      88      88     88888 88888888       88888888\n\r"	\
		"  88888888888888      88      88     88    88888888       88888888\n\r"	\
		"  88888888888888      88      88                 88       88\n\r"	\
		"\n\r");
}

FUNC(void, OS_CODE) main(void)
{
#if (OsNumberOfCores > 1)
	VAR(StatusType, AUTOMATIC) rv = E_OK;

	if (GetCoreID() == OS_CORE_ID_MASTER) {
		print_banner();

		pr_debug("osek start in main\n");
		StartCore(OS_CORE_ID_1, &rv);
		pr_debug("osek start core 1 return %d\n", rv);
	}
#else
	print_banner();
#endif
#ifdef CONFIG_AUTOSAR_MEMORY_PROTECTION
	mpu_init();
#endif
	/**
	 * 启动OSEK系统
	 */
	StartOS(OSDEFAULTAPPMODE);
}
