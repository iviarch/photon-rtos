/*
 * PhotonRTOS础光实时操作系统 -- resource测试
 *
 * Copyright (C) 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Zicheng Hu <huzicheng@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#include "../utest.h"
#include <Os.h>

TEST(GetResource)
{
	VAR(StatusType, AUTOMATIC) st;

	st = GetResource(0);
	ASSERT_EQ(st, E_OK);
	/* 测试多次获取已经获取的资源 */
	st = GetResource(0);
	ASSERT_NE(st, E_OK);

	st = ReleaseResource(0);
	ASSERT_EQ(st, E_OK);
}

TEST(ReleaseResource)
{
	VAR(StatusType, AUTOMATIC) st;
	/* 测试尚未获取的资源 */
	st = ReleaseResource(0);
	ASSERT_NE(st, E_OK);
	st = GetResource(0);
	ASSERT_EQ(st, E_OK);

	st = ReleaseResource(0);
	ASSERT_EQ(st, E_OK);
	/* 测试已经释放的资源 */
	st = ReleaseResource(0);
	ASSERT_NE(st, E_OK);
}

TEST_SUIT(Resource)
{
	UTEST_CASE(GetResource);
	UTEST_CASE(ReleaseResource);
}
