/*
 * 础光实时操作系统PhotonRTOS -- 测试
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Baoyou Xie <xiebaoyou@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#include "utest.h"
#include <Os.h>

/**
 * 导入测试用例
 */
extern TEST_SUIT(Misc);
extern TEST_SUIT(Alarm);
extern TEST_SUIT(Resource);
extern TEST_SUIT(Event);
extern TEST_SUIT(Interrupt);
extern TEST_SUIT(Task);
extern TEST_SUIT(SysLog);

extern TEST_SUIT(Counter);
extern TEST_SUIT(ScheduleTable);
extern TEST_SUIT(Peripheral);
extern TEST_SUIT(Spinlock);
extern TEST_SUIT(Multicore);
extern TEST_SUIT(Protection);
extern TEST_SUIT(OsApplication);
extern TEST_SUIT(IOC);

TASK(UnitTestTask, app0)
{
	/**
	 * 调用测试用例
	 */
	UTEST_START();
	UTEST_SUITE(Misc);
	UTEST_SUITE(Alarm);
	UTEST_SUITE(Resource);
	UTEST_SUITE(Interrupt);
	UTEST_SUITE(Task);
	UTEST_SUITE(Event);
	UTEST_SUITE(Counter);
	UTEST_SUITE(ScheduleTable);
#ifdef CONFIG_IOC
	UTEST_SUITE(IOC);
#endif
	UTEST_SUITE(Peripheral);
	UTEST_SUITE(Spinlock);
	UTEST_SUITE(Multicore);
	UTEST_SUITE(Protection);
	UTEST_SUITE(OsApplication);

	UTEST_SUITE(SysLog);
	UTEST_END();
#if defined(AUTOSAR_SHOW_TASK_INFO)
	ShowTaskInfo(BASE_INFO, ALL_TASK);
	ShowTaskInfo(EXT_INFO, ALL_TASK);
	ShowTaskInfo(BASE_INFO, 0);
	ShowTaskInfo(EXT_INFO, 0);
	/* 测试没初始化的任务 */
	ShowTaskInfo(BASE_INFO, 5);
	ShowTaskInfo(EXT_INFO, 5);
	/* 测试无效的任务ID */
	ShowTaskInfo(BASE_INFO, 1024);
	ShowTaskInfo(EXT_INFO, 1024);
#endif /* defined(AUTOSAR_SHOW_TASK_INFO) */
	ShutdownOS(E_OK);
	TerminateTask();
}

static void print_banner(void)
{
	printk("\n"	\
		"  88888888888888       88     88              88      88888888888888\n\r"\
		"  88888888888888   888888 88  88    888888 88 88 88   88888888888888\n\r"\
		"  88          88   888888  88 88    888888 88 88 88       88\n\r"	\
		"  88 88888888 88      88    8 88      88   88 88 88       88\n\r"	\
		"  88 88888888 88   8888888    88      88   88 88 88       88888888\n\r"	\
		"  88    88    88   8888888 8  88     88888 88888888      888888888\n\r"	\
		"  88 88888888 88      88   88 88    888888 88888888     8888    88\n\r"	\
		"  88    88    88     8888   8 88    888 88    88       88 88    88\n\r"	\
		"  88    88  8 88    888888    88     88 88 88 88 88   88  88    88\n\r"	\
		"  88 88888888888   88 88 888888888   88 88 88 88 88       88    88\n\r"	\
		"  88 88888888888   8  88 8888888     88888 88 88 88       88    88\n\r"	\
		"  88          88      88      88     88888 88888888       88888888\n\r"	\
		"  88888888888888      88      88     88    88888888       88888888\n\r"	\
		"  88888888888888      88      88                 88       88\n\r"	\
		"\n\r");
}

FUNC(void, OS_CODE) main(void)
{
#if (OsNumberOfCores > 1)
	VAR(StatusType, AUTOMATIC) rv = E_OK;

	if (GetCoreID() == OS_CORE_ID_MASTER) {
		print_banner();
		pr_debug("osek start in main\n");
		StartCore(OS_CORE_ID_1, &rv);
		pr_debug("osek start core 1 return %d\n", rv);
	}
#else
	print_banner();
#endif
#ifdef CONFIG_AUTOSAR_MEMORY_PROTECTION
	mpu_init();
#endif

	StartOS(OSDEFAULTAPPMODE);
}
