/*
 * 础光实时操作系统PhotonRTOS -- 故障处理
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Baoyou Xie <xiebaoyou@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#include <photon/bug_build.h>
#include <photon/kernel.h>
#include <photon/printk.h>
#include <photon/sched.h>
#include <photon/irqflags.h>

void print_warning(const char *file, const int32_t line)
{
	pr_warn("------------[ cut here ]------------\n");
	pr_warn("WARNING: CPU: %d PID: %d at %s:%d\n",
		0, current->osek_id, file, line);

	dump_stack();
}

void panic(const char *fmt, ...)
{
	printk(fmt);
	dump_stack();

	disable_irq();
	while (true)
	{
	}
}
