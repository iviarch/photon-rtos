/*
 * 础光实时操作系统PhotonRTOS -- 多核功能支持
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Baoyou Xie <xiebaoyou@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#include <photon/cpu.h>
#include <photon/cpumask.h>
#include <photon/smp.h>
#include <photon/init_integrity.h>

void init_slave_early(void)
{
	uint32_t cpu;
	int32_t i;

	MODULE_INIT(init_slave_early);
	for (i = 1; i < MAX_CPUS; i++) {
		mark_cpu_possible(i, true);
	}

	for_each_possible_cpu(cpu) {
		mark_cpu_present(cpu, true);
	}
}
