/*
 * PhotonRTOS础光实时操作系统 -- 位操作实现
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Baoyou Xie <xiebaoyou@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef __ASM_BITSPERLONG_H
#define __ASM_BITSPERLONG_H


#include <photon/bitsperlong.h>

#endif /* __ASM_BITSPERLONG_H */
