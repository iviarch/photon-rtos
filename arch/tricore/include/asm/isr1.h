/*
 * PhotonRTOS础光实时操作系统 -- ARM架构一类中断相关文件
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Yili Zhang <s-zhangyili@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */
#ifndef __ARM_ISR1_H
#define __ARM_ISR1_H

#define __WEAK__ __attribute__((weak))
#define __interrupt __attribute__((section(".intr_vector")))
#define ISR1(Name) void (*Name[NR_IRQS])(void)
#define ISR1_DEFAULT_FUNC(irqno) __WEAK__ void isr1_default_func##irqno(void)
extern ISR1(isr1);
#endif
