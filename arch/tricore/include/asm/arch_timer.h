/*
 * PhotonRTOS础光实时操作系统 -- 体系架构定时器
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Baoyou Xie <xiebaoyou@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef __ASM_ARCH_TIMER_H
#define __ASM_ARCH_TIMER_H

#include <photon/bug_build.h>
#include <photon/init.h>
#include <asm/types.h>
#include <std/Ifx_types.h>

#include <asm/barrier.h>

#define ARCH_FUN_arch_timer_get_cntfrq_DEFINED
static inline uint32_t arch_timer_get_cntfrq(void)
{
	return IfxStm_getFrequency(&MODULE_STM0);
}

static uint64 arch_get_cycles(void)
{
	return IfxStm_get(&MODULE_STM0);
}

static uint64_t arch_cycles_to_usecs(const uintptr_t cycles)
{
	return cycles / IfxStm_getTicksFromMicroseconds(&MODULE_STM0, 1);
}

/**
 * TODO：记得删，等到实现中断嵌套功能后
 * 
 */
#define read_cpuid() 0
int xilinx_ttc_int_status(int cpu, uint32_t countid);

#endif /* LIBRARIES_CHUSHI_CP_ARCH_TC397_INCLUDE_ASM_ARCH_TIMER_H_ */
