/*
 * PhotonRTOS础光实时操作系统 -- 中断头文件
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Baoyou Xie <xiebaoyou@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef __ASM_ARM_IRQ_H
#define __ASM_ARM_IRQ_H

#define ARCH_FUN_set_chip_irq_handle_DEFINED


/**
 * NR_IRQS 支持的最大中断数
 *
 */
#define NR_IRQS 192U
#ifndef __PHOTON_IRQCHIP_ARM_GIC_H
#define __PHOTON_IRQCHIP_ARM_GIC_H
#include <photon/types.h>
#define GIC_CPU_CTRL			0x00U
#define GIC_CPU_PRIMASK			0x04U
#define GIC_CPU_BPR			0x08U
#define GIC_CPU_INTACK			0x0cU
#define GIC_CPU_EOI			0x10U
#define GIC_CPU_RPR			0x14U
#define GIC_CPU_ABPR			0x1CU
#define GIC_CPU_HPPIR			0x18U

#define GICC_ENABLE			0x1U
#define GICC_INT_PRI_THRESHOLD		0xf0U
#define GICC_IAR_INT_ID_MASK		0x3ffU
#define GICC_DIS_BYPASS_MASK		0x1e0U

#define GIC_DIST_CTRL			0x000U
#define GIC_DIST_CTR			0x004U
#define GIC_DIST_IGROUPR		0x080U
#define GIC_DIST_ENABLE_SET		0x100U
#define GIC_DIST_ENABLE_CLEAR		0x180U
#define GIC_DIST_PENDING_SET		0x200U
#define GIC_DIST_PENDING_CLEAR		0x280U
#define GIC_DIST_ACTIVE_SET		0x300U
#define GIC_DIST_ACTIVE_CLEAR		0x380U
#define GIC_DIST_PRI			0x400U
#define GIC_DIST_TARGET			0x800U
#define GIC_DIST_CONFIG			0xc00U
#define GIC_DIST_SOFTINT		0xf00U
#define GIC_DIST_SGI_PENDING_CLEAR	0xf10U
#define GIC_DIST_SGI_PENDING_SET	0xf20U

#define GICD_ENABLE			0x1U
#define GICD_DISABLE			0x0U
#define GICD_INT_ACTLOW_LVLTRIG		0x0U
#define GICD_INT_EN_CLR_X32		0xffffffffU
#define GICD_INT_EN_SET_SGI		0x0000ffffU
#define GICD_INT_EN_CLR_PPI		0xffff0000U
#define GICD_INT_DEF_PRI		0xa0U
#define GICD_INT_DEF_PRI_X4		((GICD_INT_DEF_PRI << 24) |	\
					(GICD_INT_DEF_PRI << 16) |	\
					(GICD_INT_DEF_PRI << 8) |	\
					GICD_INT_DEF_PRI)
#endif
/**
 * set_chip_irq_handle
 *
 * @param handle_irq
 */
void set_chip_irq_handle(void (*handle_irq)(void *regs));
int32_t gic_set_type(uint32_t hw_irq, uint32_t type);
void gic_init_bases(int32_t irq_start, uintptr_t dist_base, uintptr_t cpu_base);

extern void arch_task_init(void *stack, void* func, void *task);

extern void gic_secondary_init(void);
extern void irq_controller_init(void);
/**
 * 一类中断handler
*/
#define INTRRUPT(Irqno) void isr1_default_func##Irqno(void)

#endif
