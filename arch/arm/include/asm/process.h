/*
 * PhotonRTOS础光实时操作系统 -- 线程描述符
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Baoyou Xie <xiebaoyou@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */
#ifndef __ASM_THREAD_INFO_H
#define __ASM_THREAD_INFO_H

#ifdef __KERNEL__
#include "stack.h"

#ifndef __ASSEMBLY__


struct process_desc;

/**
 * 体系结构特定的，任务描述符
 * 将被放入通用结构process_desc中
 */
struct arch_process_desc {
};

#define ARCH_FUN_current_proc_info_DEFINED
static inline struct process_desc *current_proc_info(void)
{
	register uintptr_t sp asm ("sp");
	return (struct process_desc *)(sp & ~(PROCESS_STACK_SIZE - 1));
}

register uintptr_t current_stack_pointer asm ("sp");

#define thread_saved_fp(tsk)				\
	((uintptr_t)(tsk->task_spot.cpu_context.fp))

#endif /* __ASSEMBLY__ */
#endif /* __KERNEL__ */
#endif /* __ASM_THREAD_INFO_H */
