/*
 * mpu
 *
 * Copyright (C) 2022, 2023 国科础石(重庆)软件有限公司
 *
 * 作者: Jiayuan Liang <liangjiayuan@kernelsoft.com>
 *
 * License terms: GNU General Public License (GPL) version 3
 *
 */

#ifndef __ASM_MPU_H
#define __ASM_MPU_H
#include <asm/sections.h>
void mpu_init(void);

void enable_memory_protection(void);
void define_data_protection_range(uint32 lowerBoundAddress, uint32 upperBoundAddress, uint8 range);
void define_code_protection_range(uint32 lowerBoundAddress, uint32 upperBoundAddress, uint8 range);
void enable_data_read(uint8 protectionSet, uint8 range);
void enable_data_write(uint8 protectionSet, uint8 range);
void enable_code_execution(uint8 protectionSet, uint8 range);


void arch_switch_mpu(void);

#endif /* __ASM_MPU_H */
