#include <asm/assembler.h>
.syntax unified

	.macro	bitop, name, instr
ENTRY(	\name		)
	ands	ip, r1, #3
#if defined(CONFIG_CPU_CORTEX_M)
	strbne	r1, [ip]
#else
	strneb	r1, [ip]		@ assert word-aligned
#endif
	
	mov	r2, #1
	and	r3, r0, #31		@ Get bit offset
	mov	r0, r0, lsr #5
	add	r1, r1, r0, lsl #2	@ Get word offset
#if __PHOTON_ARM_ARCH__ >= 7 && defined(CONFIG_SMP)
	.arch_extension	mp
	ALT_SMP(W(pldw)	[r1])
	ALT_UP(W(nop))
#endif
	mov	r3, r2, lsl r3
1:	ldrex	r2, [r1]
	\instr	r2, r2, r3
	strex	r0, r2, [r1]
	cmp	r0, #0
	bne	1b
	bx	lr
ENDPROC(\name		)
	.endm

/*
 * Atomic bit operations.
 */
 #if defined(CONFIG_CPU_CORTEX_R)
 .syntax divided
 #endif

	bitop	atomic_clear_bit, bic
	bitop	atomic_set_bit, orr


